import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataSharingService } from 'src/app/sharing/datasharing.service';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { Snack } from 'src/app/models/snack.model';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-snack-detail',
  templateUrl: './snack-detail.component.html',
  styleUrls: ['./snack-detail.component.css']
})
export class SnackDetailComponent implements OnInit {
  id: string;
  faSpinner = faSpinner;
  items: Snack[];
  snack: Snack;

  constructor(private authService: AuthService, private router: Router,
              private route: ActivatedRoute, private dataSharing: DataSharingService) {
    if (!this.authService.isLoggedIn) {
      this.router.navigate(['admin/login']);
    }

    const key = 'id';
    this.id = this.route.snapshot.params[key];

    const snacks = JSON.parse(localStorage.getItem('snacks'));

    if (snacks != null) {
      this.dataSharing.items.next(snacks);
      this.dataSharing.items.subscribe(list => {
        this.items = list;
        this.snack = list.find(x => x.id === this.id);
      });
    } else {
      this.dataSharing.items.subscribe(list => {
        this.items = list;
        this.snack = list.find(x => x.id === this.id);
      });
    }
  }

  ngOnInit(): void {
  }

}
