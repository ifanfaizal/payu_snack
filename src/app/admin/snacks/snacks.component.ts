import { Router } from '@angular/router';
import { Component, OnInit, HostListener } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { faSpinner, faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import 'firebase/firestore';
import 'firebase/storage';
import { Snack } from 'src/app/models/snack.model';
import { DataSharingService } from 'src/app/sharing/datasharing.service';
import { AuthService } from 'src/app/services/auth/auth.service';
declare function convertToRupiah(value: string): any;

@Component({
  selector: 'app-snacks',
  templateUrl: './snacks.component.html',
  styleUrls: ['./snacks.component.css']
})
export class SnacksComponent implements OnInit {
  private itemsCollection: AngularFirestoreCollection<Snack>;
  faSpinner = faSpinner;
  faEdit = faEdit;
  faTrash = faTrash;
  items$: Observable<Snack[]>;
  imageUrls: Snack[] = [];

  constructor(private authService: AuthService, private router: Router,
    private firestore: AngularFirestore, private storage: AngularFireStorage,
    private dataSharing: DataSharingService) {

    if (!this.authService.isLoggedIn) {
      this.router.navigate(['admin/login']);
    }

    this.itemsCollection = this.firestore.collection<Snack>('snacks');
    this.items$ = this.itemsCollection.valueChanges({ idField: 'id' });
    this.items$.subscribe(list => {
      this.dataSharing.items.next(list);
      localStorage.setItem('snacks', JSON.stringify(list));
      list.forEach(data => {
        const fileRef = this.storage.ref(data.imageName);
        fileRef.getDownloadURL().subscribe(a => {
          this.imageUrls.push(new Snack(data.id, a));
        });
      });
    });
  }

  ngOnInit(): void {
  }

  convertToRupiahFunc(value: string): string {
    return convertToRupiah(value);
  }

  getImageUrl(id: string): string {
    return this.imageUrls.find(x => x.id === id).imageUrl;
  }

}
