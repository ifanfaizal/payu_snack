import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-galeri',
  templateUrl: './galeri.component.html',
  styleUrls: ['./galeri.component.css']
})
export class GaleriComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) {
    if (!this.authService.isLoggedIn) {
      this.router.navigate(['admin/login']);
    }
  }

  ngOnInit(): void {
  }

}
