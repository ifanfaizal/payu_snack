import { Component, OnInit } from '@angular/core';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  faSpinner = faSpinner;
  email: string;
  password: string;
  isLoading = false;

  constructor(private authService: AuthService, private router: Router) {
    if (this.authService.isLoggedIn) {
      this.router.navigate(['admin/dashboard']);
    }
  }

  ngOnInit(): void {
  }

  async login() {
    this.isLoading = true;
    await this.authService.login(this.email, this.password);
    this.isLoading = false;
  }
}
