import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import { DataSharingService } from '../../sharing/datasharing.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: User;
  constructor(public afAuth: AngularFireAuth, public router: Router, private dataSharingService: DataSharingService) {
  }

  async login(email: string, password: string) {
    await this.afAuth.signInWithEmailAndPassword(email, password).catch((e) => {
      console.log(e);
    }).then((res: firebase.auth.UserCredential) => {
      if (res.user != null) {
        this.user = res.user;
        localStorage.setItem('user', JSON.stringify(this.user));
        this.router.navigate(['admin/dashboard']);
      } else {
        localStorage.setItem('user', null);
      }
    });
  }

  async register(email: string, password: string) {
    const result = await this.afAuth.createUserWithEmailAndPassword(email, password);
    this.sendEmailVerification();
  }

  async sendEmailVerification() {
    (await this.afAuth.currentUser).sendEmailVerification();
    this.router.navigate(['admin/verify-email']);
  }

  async sendPasswordResetEmail(passwordResetEmail: string) {
    return await this.afAuth.sendPasswordResetEmail(passwordResetEmail);
  }

  async logout() {
    localStorage.removeItem('user');
    await this.afAuth.signOut();
    this.dataSharingService.isUserLoggedIn.next(false);
  }

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));

    if (user !== null) {
      this.dataSharingService.isUserLoggedIn.next(true);
    }

    return user !== null;
  }

  async loginWithGoogle() {
    await this.afAuth.signInWithPopup(new auth.GoogleAuthProvider());
    this.router.navigate(['admin/dashboard']);
  }
}
