import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import 'firebase/firestore';
import { Snack } from '../models/snack.model';

@Injectable({
  providedIn: 'root'
})
export class SnackService {

  constructor(private firestore: AngularFirestore) {

  }

  updateSnack(snack: Snack) {
    this.firestore.doc('snacks/' + snack.id).update(snack);
  }
}
