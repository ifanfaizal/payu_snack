import { Component, OnInit } from '@angular/core';
import { faPhone, faBook, faHome, faInfoCircle,
  faPowerOff, faBars, faHotdog, faImage } from '@fortawesome/free-solid-svg-icons';
import { DataSharingService } from '../../sharing/datasharing.service';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  faPhone = faPhone;
  faImage = faImage;
  faHotdog = faHotdog;
  faBook = faBook;
  faHome = faHome;
  faBars = faBars;
  faInfoCircle = faInfoCircle;
  faPowerOff = faPowerOff;
  isLoggedIn: boolean;
  showMenu = new BehaviorSubject<boolean>(false);

  constructor(private authService: AuthService, private dataSharingService: DataSharingService, private router: Router) {
    this.dataSharingService.isUserLoggedIn.subscribe(value => {
      this.isLoggedIn = value;
    });

    this.router.events.subscribe((res) => {
      this.showMenu.next(!this.router.url.includes('admin'));
    });
  }

  async logout() {
    await this.authService.logout();
  }

  ngOnInit(): void {
  }

}
