import { Component, OnInit, HostListener } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import 'firebase/firestore';
import 'firebase/storage';
import { Snack } from 'src/app/models/snack.model';
declare function convertToRupiah(value: string): any;

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  private itemsCollection: AngularFirestoreCollection<Snack>;
  math = Math;
  faSpinner = faSpinner;
  items$: Observable<Snack[]>;
  imageUrls: Snack[] = [];
  screenSize: any;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    const scrHeight = window.innerHeight;
    const scrWidth = window.innerWidth;
    console.log(scrHeight, scrWidth);

    if (scrWidth <= 576) {
      this.screenSize = 'xs';
    } else if (scrWidth <= 768) {
      this.screenSize = 'sm';
    } else if (scrWidth <= 992) {
      this.screenSize = 'md';
    } else if (scrWidth <= 1200) {
      this.screenSize = 'lg';
    } else {
      this.screenSize = 'xl';
    }
  }

  constructor(private firestore: AngularFirestore, private storage: AngularFireStorage) {
    this.getScreenSize();
    this.itemsCollection = this.firestore.collection<Snack>('snacks');
    this.items$ = this.itemsCollection.valueChanges({ idField: 'id' });
    this.items$.subscribe(list => list.forEach(data => {
      const fileRef = this.storage.ref(data.imageName);
      fileRef.getDownloadURL().subscribe(a => {
        this.imageUrls.push(new Snack(data.id, a));
      });
    }));
  }

  ngOnInit(): void {
  }

  convertToRupiahFunc(value: string): string {
    return convertToRupiah(value);
  }

  getImageUrl(id: string): string {
    return this.imageUrls.find(x => x.id === id).imageUrl;
  }

}
