import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Snack } from 'src/app/models/snack.model';
import { AngularFireStorage } from '@angular/fire/storage';
import 'firebase/firestore';
import 'firebase/storage';
import { SnackService } from 'src/app/services/snack.service';
import { NgxSpinnerService } from "ngx-spinner";
declare var $: any;
declare function convertToAngka(value: string): any;
declare function convertToRupiah(value: string): any;

@Component({
  selector: 'app-snack-form',
  templateUrl: './snack-form.component.html',
  styleUrls: ['./snack-form.component.css']
})
export class SnackFormComponent implements OnInit {
  @Input() snackData: Snack;

  snackForm: FormGroup;
  imageUrl: string;

  constructor(private fb: FormBuilder, private storage: AngularFireStorage, private snackService: SnackService,
    private spinner: NgxSpinnerService) {

  }

  ngOnInit(): void {
    $(document).ready(() => {
      $('#snackPrice').focus(() => {
        var value = $('#snackPrice').val();
        if (value !== '')
          $('#snackPrice').val(convertToAngka(value));
      });

      $('#snackPrice').blur(() => {
        $('#snackPrice').val(convertToRupiah($('#snackPrice').val()));
      });
    });

    this.snackForm = this.fb.group({
      id: this.fb.control(this.snackData.id),
      snackName: this.fb.control(this.snackData.snackName),
      snackDesc: this.fb.control(this.snackData.snackDesc),
      snackPrice: this.fb.control(convertToRupiah(this.snackData.snackPrice.toString())),
      imageName: this.fb.control('')
    });

    const fileRef = this.storage.ref(this.snackData.imageName);
    fileRef.getDownloadURL().subscribe(a => {
      this.imageUrl = a;
    });
  }

  onSubmit() {
    this.spinner.show();

    var price = this.snackForm.value.snackPrice;
    if (price !== null && price !== '') {
      this.snackForm.value.snackPrice = convertToAngka(this.snackForm.value.snackPrice);
    }
    
    this.snackForm.value.imageName = this.snackData.imageName;
    console.warn(this.snackForm.value);
    this.snackService.updateSnack(this.snackForm.value);

    this.spinner.hide();
  }

}