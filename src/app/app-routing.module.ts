import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { MenuComponent } from './components/menu/menu.component';
import { LoginComponent } from './admin/login/login.component';
import { RegisterComponent } from './admin/register/register.component';
import { ForgotPasswordComponent } from './admin/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './admin/verify-email/verify-email.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { GaleriComponent } from './admin/galeri/galeri.component';
import { SnacksComponent } from './admin/snacks/snacks.component';
import { SnackDetailComponent } from './admin/snack-detail/snack-detail.component';

const routes: Routes = [
  { path:  '', component: HomeComponent },
  { path:  'menu', component: MenuComponent },
  {
    path: 'admin',
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'forgot-password', component: ForgotPasswordComponent },
      { path: 'verify-email', component: VerifyEmailComponent },
      { path: 'galeri', component: GaleriComponent },
      { path: 'snacks', component: SnacksComponent },
      { path: 'snacks/:id', component: SnackDetailComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
