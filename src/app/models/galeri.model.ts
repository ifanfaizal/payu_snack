export class Galeri {
    constructor(id: string, imageUrl: string) {
        this.id = id;
        this.imageUrl = imageUrl;
    }

    id: string;
    imageUrl: string;
    imageName: string;
}
