export class Snack {
    constructor(id: string, imageUrl: string) {
        this.id = id;
        this.imageUrl = imageUrl;
    }

    id: string;
    snackName: string;
    imageName: string;
    imageUrl: string;
    snackPrice: number;
    snackDesc: string;
}
