import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Snack } from '../models/snack.model';

@Injectable({
  providedIn: 'root'
})
export class DataSharingService {
  public isUserLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public items: BehaviorSubject<Snack[]> = new BehaviorSubject<Snack[]>(null);

  constructor() { }
}
