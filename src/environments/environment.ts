// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDnXTatfGYvE9zCWrLmg_ltrOie1MbEwTU',
    authDomain: 'payu-snack.firebaseapp.com',
    databaseURL: 'https://payu-snack.firebaseio.com',
    projectId: 'payu-snack',
    storageBucket: 'payu-snack.appspot.com',
    messagingSenderId: '468187062390',
    appId: '1:468187062390:web:a35f59da34530f030e3f57',
    measurementId: 'G-4RK22NGJH4'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
